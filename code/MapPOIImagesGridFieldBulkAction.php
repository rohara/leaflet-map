<?php
/**
 * @package comments
 */
class MapPOIImagesGridFieldBulkAction extends GridFieldBulkActionHandler {
	
}
/**
 * A {@link GridFieldBulkActionHandler} for bulk marking comments as spam
 *
 * @package comments
 */
class MapPOIImagesGridFieldBulkAction_Approved extends MapPOIImagesGridFieldBulkAction {
	
	private static $allowed_actions = array(
		'approved',
		'featured',
		'unapproved',
		'unfeatured'
	);
	private static $url_handlers = array(
		'approved' => 'approved',
		'featured' => 'featured',
		'unapproved' => 'approved',
		'unfeatured' => 'unfeatured'
	);
	
	public function approved(SS_HTTPRequest $request) {
		$ids = array();
		
		foreach($this->getRecords() as $record) {						
			array_push($ids, $record->ID);
			$record->Approved = 1;
			$record->write();
		}
		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'done' => true,
			'records' => $ids
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
	
	public function featured(SS_HTTPRequest $request) {
		$ids = array();
		
		foreach($this->getRecords() as $record) {						
			array_push($ids, $record->ID);
			$record->Featured = 1;
			$record->write();
		}
		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'done' => true,
			'records' => $ids
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
	
	public function unapproved(SS_HTTPRequest $request) {
		$ids = array();
		
		foreach($this->getRecords() as $record) {						
			array_push($ids, $record->ID);
			$record->Approved = 0;
			$record->write();
		}
		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'done' => true,
			'records' => $ids
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
	
	public function unfeatured(SS_HTTPRequest $request) {
		$ids = array();
		
		foreach($this->getRecords() as $record) {						
			array_push($ids, $record->ID);
			$record->Featured = 0;
			$record->write();
		}
		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'done' => true,
			'records' => $ids
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
}