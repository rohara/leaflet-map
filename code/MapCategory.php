<?php
class MapCategory extends DataObject{

	public static $singular_name = 'Map Category';

	public static $db = array(
		'Title' => 'Varchar(256)',
		'Colour' => 'Varchar(6)',
		'SortOrder' => 'Int'
	);
	
	public static $has_one = array(
		'MapCategoryImage' => 'Image',
		'MapCategoryHolder' => 'MapCategoryHolder'
	);
	
	public static $has_many = array(
		'MapPOIs' => 'MapPOI'
	);
	
	public static $summary_fields = array(
		'Title'
	);
	
	static $default_sort = "SortOrder ASC";
	
	public function getCMSFields(){
		$fields = parent::getCMSFields();
		
		$fields->removeByName("SortOrder");
		$fields->removeByName("MapCategoryHolderID");
		$fields->removeByName("MapPOIs");
		$fields->addFieldToTab("Root.Main", new TextField("Title","Title"));
		$fields->addFieldToTab("Root.Main", $uploadfield = new UploadField("MapCategoryImage","Category Image"));
		$uploadfield->setFolderName("MapCategory");
		
		$fields->addFieldToTab("Root.Main", new ColorField("Colour","Colour"));
		
		if($this->exists()){
			$gridfieldPages = new GridField("MapPOIs","Map POIs",$this->MapPOIs());
			$gridfieldPages->getConfig()
			     ->addComponent(new GridFieldDetailForm())
			     ->addComponent(new GridFieldAddNewButton('toolbar-header-right'))
			     ->addComponent(new GridFieldEditButton())
			     ->addComponent(new GridFieldDeleteAction())
			     ->addComponent(new GridFieldSortableRows('SortOrder'));
			$fields->addFieldToTab("Root.Main", $gridfieldPages);
		}
		
		return $fields;
	}

}