<?php
class MapCategoryHolder extends DataObject{

	public static $singular_name = 'Map Category Holder';

	public static $db = array(
		'Title' => 'Varchar(256)',
		'SortOrder' => 'Int',
		'ShowHide' => 'Boolean'
	);
	
	public static $has_one = array(
		'MapPage' => 'MapPage'
	);
	
	public static $has_many = array(
		'MapCategories' => 'MapCategory'
	);
	
	public static $summary_fields = array(
		'Title'
	);
	
	static $default_sort = "SortOrder ASC";
	
	public function getCMSFields(){
		$fields = parent::getCMSFields();
		
		$fields->removeByName("SortOrder");
		$fields->removeByName("MapPageID");
		$fields->removeByName("ShowHide");
		$fields->addFieldToTab("Root.Main", new TextField("Title","Title"));
		$fields->addFieldToTab("Root.Main", FieldGroup::create(
			CheckBoxField::create("ShowHide", "Do you want to display this list on the map?"),
			LiteralField::create('ShowHideMessage', '<div class="field checkbox"><strong>DO NOT check if this is not the Trails holder</strong></div>')
		));
		// above is messy way of trying to push the literal field under the checkbox - purely asthetic

		if($this->exists()){
			$gridfieldPages = new GridField("MapCategories","Map Categories",$this->MapCategories());
			$gridfieldPages->getConfig()
			     ->addComponent(new GridFieldDetailForm())
			     ->addComponent(new GridFieldAddNewButton('toolbar-header-right'))
			     ->addComponent(new GridFieldEditButton())
			     ->addComponent(new GridFieldDeleteAction())
			     ->addComponent(new GridFieldSortableRows('SortOrder'));
			$fields->addFieldToTab("Root.Main", $gridfieldPages);
		}
		return $fields;
	}

}