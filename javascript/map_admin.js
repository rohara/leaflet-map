jQuery(function($) {
	
	// baseURL
	var baseURL;
	var mapID;
	var MarkerImageID;
	var map;
	
	var imgwidth = 4694;
	var imgheight = 2576;

	if($.entwine){
		$.entwine('ss', function($) {
			$('#admin-map-image').entwine({
				onmatch: function() {
					loadMap();
					//setPos();
					//makeDrag();
				}
			});
			
			$('#admin-map-image').entwine({
				redraw: function() {
					loadMap();
					//setPos();
					//makeDrag();
				}
			});
		});
	}
	
	function loadMap(){
		// create the slippy map
		baseURL = $("base").attr("href");
		mapID  = $("#admin-map-image").attr("map-id");
		MarkerImageID  = $("#admin-map-image").attr("marker");
		
		map = L.map('admin-map-image', {
			  minZoom: 1.5,
			  maxZoom: 4,
			  center: [0, 0],
			  zoom: 1.5,
			  crs: L.CRS.Simple,
		});
		
		get("/api/MapPage/"+mapID,loadBaseMap);
	}
	
	// load main map
	function loadBaseMap(json){
		var imageURL = getImage(json.MapImage,imgwidth,imgheight,"cropped",createBaseMap,json);
	}
	
	function createBaseMap(imageURL,json){
		
		var w = imgwidth,
		    h = imgheight,
		    url = imageURL;

		var southWest = map.unproject([0, h], map.getMaxZoom()-1);
		var northEast = map.unproject([w, 0], map.getMaxZoom()-1);
		var bounds = new L.LatLngBounds(southWest, northEast);

		L.imageOverlay(url, bounds).addTo(map);
		
		map.setMaxBounds(bounds);
		
		createImageMarker();

	}

	
	function createImageMarker(){

		L.Icon.Default.imagePath = 'leaflet-map/css/images';
		
		var icon = L.divIcon({
        	className: 'map-marker',
        	iconSize:null,
        	html:'<div class="icon" style="background-color: #'+$("#admin-map-image").attr("cat-colour")+'"></div>'
        });
		
		var marker = L.marker([51.5, -0.09],{draggable:'true', icon: icon}).addTo(map);
		if( $("#Form_ItemEditForm_XPos").val() ){
		    var lat = ($("#Form_ItemEditForm_XPos").val());
		    var lng = ($("#Form_ItemEditForm_YPos").val());
		    var newLatLng = new L.LatLng(lat, lng);
		    marker.setLatLng(newLatLng); 
		}else{
			marker.setLatLng(map.getCenter());
		}
        
        marker.on('dragend', function(event){
            var marker = event.target;
            var position = marker.getLatLng();
            $("#Form_ItemEditForm_XPos").val(position.lat);
			$("#Form_ItemEditForm_YPos").val(position.lng);
            console.log(position);
        });
        
	}
	
	/* ajax functions */
	
	/* basic get */
	function get(url,callback){
		
		var url = baseURL+url;
		
		$.getJSON( url )
		  .done(function( json ) {
		    callback(json);
		  })
		  .fail(function( jqxhr, textStatus, error ) {
		    var err = textStatus + ", " + error;
		    console.log( "Request Failed: " + err );
		});

	}
	
	/* get image */
	function getImage(id,width,height,sizetype,callback,json){

		if(id){
			var url = baseURL+"api/ResizedImage/"+id+"?ResizedMethod="+sizetype+"&Width="+width+"&Height="+height;
			$.get( url )
			  .done(function( imageURL ) {
				  callback(imageURL,json);
			  })
			  .fail(function( jqxhr, textStatus, error ) {
			    var err = textStatus + ", " + error;
			    console.log( "Request Failed: " + err );
			});
		}
	}

	
	function makeDrag(){
		$('.dragabble').pep({
			stop: function(e){
				$("#Form_ItemEditForm_XPos").val($(".dragabble").position().left + ($(".dragabble").width()/2));
				$("#Form_ItemEditForm_YPos").val($(".dragabble").position().top + $(".dragabble").height());
			}
		});
	}
	
	function setPos(){
		
		var xpos = parseInt($("#Form_ItemEditForm_XPos").val());
		var ypos = parseInt($("#Form_ItemEditForm_YPos").val());
		
		if(xpos > 0){
			$(".dragabble").css({"left":xpos - ($(".dragabble").width()/2)});
		}
		
		if(ypos > 0){
			$(".dragabble").css({"top":ypos - $(".dragabble").height()});
		}
	}
	
});



