jQuery.noConflict();

(function($) {

		// baseURL
		var baseURL = $("base").attr("href");
		var pageURL = baseURL+$("#MainMap").attr('map-page-url');
		var mapID  = $("#MainMap").attr("map-id");
		
		var MapCategoryHolders;
		var MapCategoryLayers = [];
		
		var centerx = 0;
		var centery = 0;
		var zoom = 1.7;
		
		var imgwidth = 4694;
		var imgheight = 2576;
		
		if(isMobile){
			centerx = 250;
			centery = 0;
			zoom = 1.2;
		}

		// create the slippy map
		var map = L.map('MainMap', {
		  minZoom: 1.7,
		  maxZoom: 4,
		  center: [centery,centerx],
		  zoom: zoom,
		  crs: L.CRS.Simple,
		  scrollWheelZoom: false
		});

		/* This somewhat fixes the issue of preventing scrolling of the map until it is focuesd */
		// map.once('focus', function() { map.scrollWheelZoom.enable(); });

		/*********************************
		** Test to see if we're on mobile
		** If not then we want to display the map straight away
		**********************************/

		if (!isMobile()) {
			get("/api/MapPage/"+mapID,loadBaseMap);
		}

		/***********************************************
		** If we're on mobile we have different functionality
		** So when we click the open map button 
		** - hide the instructions
		** - show the map
		** - run the get function to load all the map points
		** - append a close button to the map
		***********************************************/
	
		$('.mobile-open-map').on('click', function(e){
			e.preventDefault();
			$('.map-display-image__instruction').hide();
			$('#MainMap').show();
			get("/api/MapPage/"+mapID,loadBaseMap);


			// slide to top of map container 

			$('html,body').animate({
			  		scrollTop: $('#MainMap').offset().top
   				}, 0);
					
			// create a close button so we can exit the map
			var closeBtn = "<div id='MobileMapClose'></div>";
			$('#MainMap').append(closeBtn);
		});

		/********************************************
		** Close Map function on mobile
		********************************************/
		$('html').on('click', '#MobileMapClose', function(){
			$('#MainMap').hide();
			$('.map-display-image__instruction').show();
		});


		// load main map
		function loadBaseMap(json){
			var imageURL = getImage(json.MapImage,imgwidth,imgheight,"cropped",createBaseMap,json);
		}
		
		function createBaseMap(imageURL,json){
			
			var w = imgwidth,
			    h = imgheight,
			    url = imageURL;

			var southWest = map.unproject([0, h], map.getMaxZoom()-1);
			var northEast = map.unproject([w, 0], map.getMaxZoom()-1);
			var bounds = new L.LatLngBounds(southWest, northEast);

			L.imageOverlay(url, bounds).addTo(map);

			map.setMaxBounds(bounds);
			
			loadCategoryHolders();
		}
		
		// load category holders
		
		function loadCategoryHolders(){
			get("/api/MapCategoryHolder?MapPageID="+mapID,createCategoryHolders);
		}
		
		function createCategoryHolders(json){
			for(var i=0; i<json.length; i++){
				if(json[i].MapCategories){
					for(var c=0; c<json[i].MapCategories.length; c++){
						loadCategories(json[i].MapCategories[c]);
					}
				}
			}
		}
		
		// load categories
		
		function loadCategories(categoryHolderID){
			get("/api/MapCategory?MapCategoryHolderID="+categoryHolderID,createCategories);
		}
		
		function createCategories(json){
			for(var i=0; i<json.length; i++){

				if(json[i].MapCategoryImage){
					var imageURL = getImage(json[i].MapCategoryImage,imgwidth,imgheight,"cropped",createCategoryImage,json[i]);
				}
				// load poi's
				loadPOIs(json[i].ID);
			}
		}
		
		function createCategoryImage(imageURL,json){

			
			var w = imgwidth,
			    h = imgheight,
			    url = imageURL;
			 
			var southWest = map.unproject([0, h], map.getMaxZoom()-1);
			var northEast = map.unproject([w, 0], map.getMaxZoom()-1);
			var bounds = new L.LatLngBounds(southWest, northEast);

			categoryLayer = L.imageOverlay(url, bounds);
			categoryLayer.CategoryHolderID = json.CategoryHolderID;
			categoryLayer.CategoryID = json.ID;
			
			MapCategoryLayers.push(categoryLayer);
			
			// loadPOIs(json.ID);

		}
		
		// load pois
		
		function loadPOIs(categoryID){
			get("/api/MapPOI?MapCategoryID="+categoryID,createPOIs);
		}
		
		function createPOIs(json){
			for(var i=0; i<json.length; i++){
				createPOIMarker(json[i]);
			}
		}
			
		function createPOIMarker(json){

            L.Icon.Default.imagePath = 'leaflet-map/css/images';
            
            var icon = L.divIcon({
            	className: 'map-marker',
            	iconSize:null,
            	html:'<div class="icon" style="background-color: #'+json.Colour+'"></div>'
            });
            
    		var marker = L.marker([51.5, -0.09],{icon: icon}).on('click', onClick).addTo(map);
    		//marker.bindPopup("<b>Hello world!</b><br>I am a popup.");
    		marker.POIID = json.ID;
    		marker.categoryID = json.MapCategory;
    		// console.log(marker);
    		var lat = (json.XPos);
 		    var lng = (json.YPos);
 		    var newLatLng = new L.LatLng(lat, lng);
 		    marker.setLatLng(newLatLng); 
 		    
 		   function onClick(e) {

 			  $("#Form_CreatePOIImageForm_MapPOIID").val(this.POIID);

 			  var markerPos = this.getLatLng();
 			  map.setView(this.getLatLng(), 2);
 			  
 			   // load POI infos
 			  loadPOIImages(this.POIID);
 			  
 			   // Show Image Slider
 			  $('.map-display-image__slider').css("display", "block");
 			  // Close Image Upload Instuctions
 			  $('.map-display-image__instruction').css("display", "none");
 			  
 			  if(!isMobile()){
 			  	$('.map-thumb-img-container').slideDown();
 			  };
 			  

			   if(isMobile()) {
		 			var smoothScrollTime = 0;
		 			var carouselHeight = $('.carousel').css('height');
		 			
					$('html,body').animate({
		 			  		scrollTop: $('.map-display-image__slider').offset().top
		       				}, smoothScrollTime);
 			   }
 		   }
 		   
 		  marker.bindPopup("<span>ADD/VIEW IMAGES</span>");
 	        marker.on('mouseover', function (e) {
 	            this.openPopup();
 	        });
 	        marker.on('mouseout', function (e) {
 	            this.closePopup();
 	        });
 		 
			
		}
		
		function loadPOIImages(POIID){
			get("/api/MapPOIImage?MapPOIID="+POIID+"&Approved=1",createPOIImages);
		}
		
		function createPOIImages(json){
			$('#MapPOImages').slickRemoveAll();
			$('#MapPOIBigImages').slickRemoveAll();

			for(var i=0;i<json.length; i++){

				var imageURL = getImage(json[i].Image,100,100,"cropped",createPOIThumbnailImage,json[i]);
				var html = "<div id='MapPOIImageSliderItem_"+json[i].ID+"' class='MapPOIImageSliderItem' poi-image-id='"+json[i].ID+"'></div>";
				$('#MapPOImages').slickAdd(html);
				
				var top 			= "<div id='MapPOIImageBigItem_"+json[i].ID+"' class='MapPOIImageBigItem' poi-image-id='"+json[i].ID+"'>"
									+	 "<div class='map-big-images-text-outer-container container-1-3'>"
									+	 	"<div class='map-big-images-text-container'>"
				var trailTitle 		= 			"<h3>"+json[i].CategoryTitle+"</h3>";
				var author 			= 			"<h4><span>Rider: </span>"+json[i].Author+"</h4>";
				var date 			= 			"<h4><span>"+json[i].FormatedDate+"</span></h4>";
				var photographer 	= 			"<h4><span>Photo: </span>"+json[i].Photographer+"</h4>";
				var bottom 			= 		"</div>"
									+		 "<div class='map-poi-form-container_"+i+"'></div>"
									+ 	"</div>"
									+ "</div>";

				var bigImageURL = getImage(json.Image,800,800,"setheight",createPOIBigImage,json);

				var html = top + trailTitle;

				if (json[i].Author != null) {
					html += author;
				}

				if (json[i].Photographer != null) {
					html += photographer;
				}

				html += bottom;

				$('#MapPOIBigImages').slickAdd(html);
			}
		}
		
		function createPOIThumbnailImage(imageURL,json){
			//var html = "<div class='MapPOIImageSliderItem' poi-image-id='"+json.ID+"'><img src='"+imageURL+"'/></div>";
			$("#MapPOIImageSliderItem_"+json.ID).html("<img src='"+imageURL+"'/>");
			var bigImageURL = getImage(json.Image,800,800,"setheight",createPOIBigImage,json);
				
			
		}

		function setToPortraitOrLandscape() {
			var sliderImg = $('.slick-active .map-slider-image');
			// console.log(sliderImg);
			if (sliderImg.width() > sliderImg.height()){
				// console.log('landscape');
			    $('.map-display-image').removeClass('map-display-image__portrait-container').addClass('map-display-image__landscape-container');
			} else {
				// console.log('portrait');
				$('.map-display-image').removeClass('map-display-image__landscape-container').addClass('map-display-image__portrait-container');
			}
		}
		
		function createPOIBigImage(imageURL,json){
			var imageHtml = "<div class='container-2-3 container-last map-slider-image-container'><img src='"+imageURL+"' class='map-slider-image midway-horizontal'/></div>";

			$("#MapPOIImageBigItem_"+json.ID).append(imageHtml);
			//var html = "<div class='MapPOIImageBigItem' poi-image-id='"+json.ID+"'><h3>"+json.CategoryTitle+"</h3><h4>"+json.Author+"</h4><h4>"+json.FormatedDate+"</h4><img src='"+imageURL+"'/></div>";
			//$('#MapPOIBigImages').slickAdd(html);
			
			imagesLoaded('.map-slider-image', function() {
				Midway();
			});
		}
		
		/* ajax functions */
		
		/* basic get */
		function get(url,callback){
			
			var url = baseURL+url;
			
			$.getJSON( url )
			  .done(function( json ) {
			    callback(json);
			  })
			  .fail(function( jqxhr, textStatus, error ) {
			    var err = textStatus + ", " + error;
			    console.log( "Request Failed: " + err );
			});

		}
		
		/* get image */
		function getImage(id,width,height,sizetype,callback,json){

			if(id){
				var url = baseURL+"api/ResizedImage/"+id+"?ResizedMethod="+sizetype+"&Width="+width+"&Height="+height;
				$.get( url )
				  .done(function( imageURL ) {
					  callback(imageURL,json);
				  })
				  .fail(function( jqxhr, textStatus, error ) {
				    var err = textStatus + ", " + error;
				    console.log( "Request Failed: " + err );
				});
			}
		}

		function isMobile() {
			// Mobile Breakpoint    
			var deviceWidthSm = 768;
			var isMobile = false;

			if ($(window).width() < deviceWidthSm) {
				isMobile = true;
			}

			return isMobile;
		}
		
		
		$( document ).ready(function() {
			
			baseURL = $("base").attr("href");
			pageURL = baseURL+$("#MainMap").attr('map-page-url');
			mapID  = $("#MainMap").attr("map-id");
			
			var formAction = $('.map-poi-form form').attr('action');

			/* POI Image Form */
			$("#Form_CreatePOIImageForm").submit(function(e){
				
				e.preventDefault();

				if(!$(".input-attached-file").val()){
					alert("Oops your photo might still be uploading, please wait a moment and then submit.");
					return false;
				}
				
				$("#Form_CreatePOIImageForm").hide();
				$("#MapPOILoader").show();
				
				values = {
					   Name: $('#Form_CreatePOIImageForm_Name').val(),
					   Photographer: $('#Form_CreatePOIImageForm_Photographer').val(),
					   AuthorEmail: $('#Form_CreatePOIImageForm_AuthorEmail').val(),
					   MapPOIID: $('#Form_CreatePOIImageForm_MapPOIID').val(),
					   Image: $(".input-attached-file").val()
				};
				
	  			$.post(baseURL+"home/doCreatePOIImageForm",values,function(data){
	  				$("#MapPOISuccess").show("fast", function(){
	  					$('#ImageDropzone').data('dropzoneInterface').clear();
	  					// $("#Form_CreatePOIImageForm").fadeIn();
	  				});
	  				$('#MapPOIUpload').hide();
					$("#MapPOILoader").hide();
	 			});
				
			});

		   $(".map-cat-changer").click(function(e){
			   var catID = $(this).attr("cat-id");
			   
			   var hex = $(this).find(".menu-circle").css('border-color');

			   for(var i=0; i<MapCategoryLayers.length; i++){
				   
				   if(catID == MapCategoryLayers[i].CategoryID){ 
					   map.removeLayer(MapCategoryLayers[i]);
					   if($(this).hasClass("active")){
						   map.removeLayer(MapCategoryLayers[i]);
						   $(this).removeClass("active");
						   $(this).find(".menu-circle").css({"background-color": "transparent"});
					   }else{
						   map.addLayer(MapCategoryLayers[i]);
						   $(this).find(".menu-circle").css({"background-color": hex});
						   $(this).addClass("active");
					   }
				   }
			   }
			   e.preventDefault();
		   });

		   // Converts label to placeholder for form inputs
		   // Code modified from https://gist.github.com/kylerush/8f2717705ee172a3533f
		   $('form :input').each(function(index, elem) {
 
			    var eId = $(elem).attr('id');
			    var label = null;
			 
			    if (eId && (label = $(elem).parents('form').find('label[for='+eId+']')).length === 1) {
			        $(elem).attr('placeholder', $(label).html());
			        $(label).addClass('form-hide-label');
			    }
			  
			 });
			
			// When image slider closes return the form back to its old position
			$('.map-display-image__slider .button-close').on('click', function () {
				// console.log('close')
				POIForm = $('.map-poi-form-container').detach().first();
				// console.log(POIForm);
				$('.map-display-image__slider .container').prepend(POIForm);
				// POIForm.hide();
				// console.log($('.map-display-image__slider .container'));
				//$('.map-thumb-img-container').slideUp();
			});
		   
		   
		   $('#MapPOImages').slick({
		    	  speed: 300,
		    	  slidesToShow: 6,
		    	  slidesToScroll: 1,
		    	  asNavFor: '#MapPOIBigImages',
		    	  focusOnSelect: true,
		    	  centerMode: true,
		    	  infinite: true,
		    	  responsive: [
		    	    {
		    	      breakpoint: 1024,
		    	      settings: {
		    	        slidesToShow: 4,
		    	        slidesToScroll: 1,
		    	        dots: false,
		    	        centerMode: true,
		    	      }
		    	    }
		    	  ]
		    });
		   
		   var 	POIForm;
		   var  POIFormCopy = $('.map-poi-form-container').clone();
		   $('#MapPOIBigImages').slick({
		    	  dots: false,
		    	  arrows: false,
		    	  speed: 300,
		    	  slidesToShow:1,
		    	  slidesToScroll: 1,
		    	  asNavFor: '#MapPOImages',
		    	  onReInit: function (slider) {
		    	  	if( (slider.slideCount != 0) && (!isMobile()) ) {

			    	  	var currentContainer = $('.slick-active .map-poi-form-container_' + slider.currentSlide);
			    	  
			    	  	// If the container has children then DO NOT add a form
			    	  	if (currentContainer.has('.map-poi-form-container').length == 0) {
			    	  		POIForm = $('.map-poi-form-container').detach();
			    	  		if ($(POIForm).length == 0) {
			    	  			POIForm = POIFormCopy;
			    	  		}
		    	  			currentContainer.prepend(POIForm);
		    	  			POIForm.show();
		    	  		}
		    	  	}
		    	  },
		    	  onAfterChange: function(slider) {

		    	  	if (!isMobile()) {
	    	  			var currentContainer = $('.slick-active .map-poi-form-container_' + slider.currentSlide);
	    	  			if ($(POIForm).length == 0) {
			    	  			POIForm = POIFormCopy;
			    	  	}
	    	  			currentContainer.prepend(POIForm);
	    	  			setToPortraitOrLandscape();
    	  			}
		    	  },
		       	  responsive: [
		    	    {
		    	      breakpoint: 768,
		    	      settings: {
		    	      	dots: false,
		    	      	arrows: false,
		    	        slidesToShow: 1,
		    	        slidesToScroll: 1,
		    	        centerPadding: 0	
		    	      }
		    	    }
		    	  ]
		    });		   
		});

	    $('html').on('click', '.MapPOIImageSliderItem', function() {
			// Show Image Slider
			$('.map-display-image__slider').css("display", "block");
			var currentContainer = $('.slick-active .map-poi-form-container_' + $(this).attr('index'));
			POIForm = $('.map-poi-form-container').detach();
			if ($(POIForm).length == 0) {
					POIForm = POIFormCopy;
			}
			// currentContainer.prepend(POIForm);
			$('.map-poi-form-container').show();
	    });
		
}(jQuery));